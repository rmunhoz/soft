<?php

class Conntest_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function getConnTestResult() {
        $result = $this->db->get('conntest')->result();
        return $result;
    }

}