<?php

class Empresa_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function getCount() {
        return $this->db->count_all_results('empresas');
    }

    public function getEmpresas($id = NULL) {
        if ($id == NULL) {
            return $this->db->get('empresas')->result_array();
        } else {
            return $this->db->get_where('empresas', array('id' => $id))->result_array();
        }
    }

    public function delete($id) {
        if (!isset($id)) {
            return null;
        } else {
            $this->db->delete('empresas', array("id" => $id));
            return $this->db->affected_rows() > 0;
        }
    }

    public function insert($nome, $returnId = false) {
        if (isset($nome) && $nome != "") {
            $this->db->insert('empresas', array("nome" => $nome));
            if ($returnId) {
                return $this->db->insert_id();
            } else {
                return $this->db->affected_rows() > 0;
            }
        }
    }

    public function update($id, $nome) {
        if (isset($id) && isset($nome) && $id != "" && $nome != "") {
            $this->db->where('id', $id);
            $this->db->order_by("nome", "asc");
            $this->db->update('empresas', array("nome" => $nome));
            return $this->db->affected_rows() > 0;
        } else {
            return null;
        }
    }

}
