<?php

class Usuario_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function create($info, $returnId = TRUE) {
        $info['datacadastro'] = date('Y-m-d');
        $info['senha'] = password_hash($info['senha'], PASSWORD_BCRYPT);
        $this->db->insert('users', $info);
        if (!$returnId) {
            return $this->db->affected_rows() > 0;
        } else {
            if ($this->db->affected_rows() > 0) {
                return $info['login'];
            } else {
                return false;
            }
        }
    }

    public function userExists($login) {
        $query = $this->db->get_where('users', array('login' => $login));
        return $query->num_rows() > 0;
    }

    public function generate_token($login) {
        if ($this->userExists($login)) {
            $token = bin2hex(openssl_random_pseudo_bytes(rand(10, 15)));
            $this->db->update('tokens', Array("valid" => 0), Array("user" => $login));
            $values = Array(
                "token" => $token,
                "valid_to" => date('Y-m-d H:i:s', strtotime('+1 hour')),
                "user" => $login
            );
            $this->db->insert('tokens', $values);
            if ($this->db->affected_rows() > 0) {
                return Array(
                    "success" => true,
                    "message" => "Um link de redefinição de senha foi enviado ao seu email.",
                    "token" => $token,
                    "login" => $login
                );
            } else {
                return Array(
                    "success" => false,
                    "message" => "Erro ao criar o token para redinir a senha; por favor, tente novamente mais tarde"
                );
            }
        } else {
            return Array(
                "success" => false,
                "message" => "Usuário não encontrado em nosso banco de dados"
            );
        }
    }

    public function validate_token($token) {
        $this->db->where('token', $token);
        $this->db->where('valid', '1');
        $this->db->where('valid_to >= now()');
        $tokenregs = $this->db->get('tokens')->result();
        if (count($tokenregs) > 0) {
            return true;
        } else {
            return Array(
                "success" => false,
                "message" => "Token inválido!"
            );
        }
    }

    public function passwd($token, $senha) {
        $this->db->where('token', $token);
        $this->db->where('valid', '1');
        $this->db->where('valid_to >= now()');
        $tokenreg = $this->db->get('tokens')->result();
        if (count($tokenreg) > 0) {
            $tokenreg = $tokenreg[0];
            $this->db->update('tokens', Array("valid" => '0'), Array("token" => $token));
            if ($this->db->affected_rows() > 0) {
                $this->db->update('users', Array("senha" => password_hash($senha, PASSWORD_BCRYPT)), Array("login" => $tokenreg->user));
                if ($this->db->affected_rows() > 0) {
                    return Array(
                        "success" => true,
                        "message" => "Senha restaurada com sucesso"
                    );
                } else {
                    return Array(
                        "success" => false,
                        "message" => "Erro ao alterar a senha; por favor, tente novamente",
                        "tokenreg" => $tokenreg
                    );
                }
            }
        } else {
            return Array(
                "success" => false,
                "message" => "Token inválido!"
            );
        }
    }

    public function get_users() {
        $this->db->select('CONCAT(`nome`, \' \', `sobrenome`) as `nome`, `login`, `email`, IF(`admin`, "Sim", "Não") as `admin`');
        return $this->db->get('users')->result_array();
    }

    public function delete($login) {
        $this->db->delete('users', Array('login' => $login));
        return $this->db->affected_rows() > 0;
    }

}
