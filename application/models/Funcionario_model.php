<?php

class Funcionario_model extends CI_Model {

    private $tables;

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->tables = Array(
            "setores" => $this->db->dbprefix('setores'),
            "funcionarios" => $this->db->dbprefix('funcionarios'),
            "empresas" => $this->db->dbprefix('empresas')
        );
    }

    public function getAniversariantes() {
        $this->db->select("{$this->tables['funcionarios']}.nome, sobrenome, funcao, DAY(data_nascimento) as dia_nascimento, {$this->tables['setores']}.nome as setor, TIMESTAMPDIFF(YEAR, data_nascimento, concat_ws('-', YEAR(now()), MONTH(data_nascimento), DAY(data_nascimento))) as idade");
        $this->db->where('MONTH(data_nascimento) = MONTH(NOW())');
        $this->db->join("setores", "{$this->tables['setores']}.id = {$this->tables['funcionarios']}.setor");
        $query = $this->db->get('funcionarios');
        return $query->result_array();
    }

    public function getCount() {
        $this->db->where('ativo = 1');
        return $this->db->count_all_results('funcionarios');
    }

    public function getInfosBasicas($id = NULL) {
        $this->db->select("{$this->tables['funcionarios']}.id, {$this->tables['funcionarios']}.nome, sobrenome, funcao, {$this->tables['setores']}.nome as setor, {$this->tables['empresas']}.nome as empresa, ativo");
        if ($id != NULL) {
            $this->db->where(array("id" => $id));
        }
        $this->db->join("{$this->tables['setores']}", "{$this->tables['setores']}.id = {$this->tables['funcionarios']}.setor");
        $this->db->join("empresas", "{$this->tables['empresas']}.id = {$this->tables['funcionarios']}.empresa");
        $query = $this->db->get('funcionarios');
        return $query->result_array();
    }

    public function getDetails($id = NULL, $join = TRUE) {
        /* @var $query CI_DB_result */
        if ($join) {
            $this->db->select("{$this->tables['funcionarios']}.id, {$this->tables['funcionarios']}.nome, sobrenome, endereco, numero, complemento, bairro, municipio, cep, pai, mae, data_nascimento, local_de_nascimento, estado_civil, sexo, escolaridade, telefone, celular, email, ctps, rg, cpf, titulo_de_eleitor, habilitacao, reservista, vale_transporte, obs, data_de_admissao, tempo_experiencia, funcao, horario, salario, matricula, ativo, {$this->tables['empresas']}.nome as empresa, {$this->tables['setores']}.nome as setor");
            $this->db->join("setores", "{$this->tables['setores']}.id = {$this->tables['funcionarios']}.setor");
            $this->db->join("empresas", "{$this->tables['empresas']}.id = {$this->tables['funcionarios']}.empresa");
        }
        $this->db->where(array("{$this->tables['funcionarios']}.id" => $id));
        $query = $this->db->get('funcionarios');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    public function delete($id = null) {
        if ($id != false && $id != "") {
            $this->db->delete('funcionarios', array('id' => $id));
            return $this->db->affected_rows() > 0;
        } else {
            return false;
        }
    }

    public function batchDelete(Array $ids = null) {
        if (is_array($ids)) {
            $this->db->where_in('id', $ids);
            $this->db->delete('funcionarios');
            return $this->db->affected_rows() > 0;
        } else {
            return false;
        }
    }

    public function inactivate($id) {
        if (is_array($id)) {
            $this->db->where_in('id', $id);
        } else {
            $this->db->where('id', $id);
        }
        $this->db->update('funcionarios', array("ativo" => 0));
        return $this->db->affected_rows() > 0;
    }

    public function activate($id) {
        if (is_array($id)) {
            $this->db->where_in('id', $id);
        } else {
            $this->db->where('id', $id);
        }
        $this->db->update('funcionarios', array("ativo" => 1));
        return $this->db->affected_rows() > 0;
    }

    public function update($info) {
        $id = isset($info['id']) ? $info['id'] : null;
        if ($id != null) {
            unset($info['id']);
            $info['data_nascimento'] = implode("-", array_reverse(explode("/", $info['data_nascimento'])));
            $info['data_de_admissao'] = implode("-", array_reverse(explode("/", $info['data_de_admissao'])));
            $this->db->where('id', $id);
            $this->db->update('funcionarios', $info);
            return ($this->db->affected_rows() > 0);
        } else {
            return false;
        }
    }

    public function create($info, $resturnId = FALSE) {
        $info['data_nascimento'] = implode("-", array_reverse(explode("/", $info['data_nascimento'])));
        $info['data_de_admissao'] = implode("-", array_reverse(explode("/", $info['data_de_admissao'])));
        $info['ativo'] = 1;
        $this->db->insert('funcionarios', $info);
        if (!$returnId) {
            return $this->db->affected_rows() > 0;
        } else {
            if ($this->db->affected_rows() > 0) {
                return $this->db->insert_id();
            } else {
                return false;
            }
        }
    }

}
