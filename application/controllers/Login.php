<?php

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("auth_model");
    }

    public function index() {
        $logado = $this->session->userdata("user");

        if (!(isset($logado)) || $logado == "") {
            $data['page_title'] = "Login";
            $data['extras'] = array(link_tag(base_url('assets/css/login.css')));
            $this->load->view("templates/header", $data);
            $error = $this->session->flashdata('login_error');
            $message = $this->session->flashdata('login_message');
            if (isset($error)) {
                $data['error'] = $error;
            }
            if (isset($message)) {
                $data['message'] = $message;
            }
            $this->load->view("login/form", $data);
            $this->load->view("templates/footer");
        } else {
            redirect(base_url());
        }
    }

    public function doLogin() {
        $login = $this->input->post('login');
        $senha = $this->input->post('senha');
        $user = $this->auth_model->getUser($login, $senha);
        if (isset($user)) {
            $this->session->set_userdata('user', $user);
            redirect(base_url());
        } else {
            $this->session->set_flashdata('login_error', 'Usuário ou senha incorretos');
            redirect(base_url('login'));
        }
    }

}
