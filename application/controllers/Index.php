<?php

class Index extends MY_Controller {

    private $user;

    function __construct() {
        parent::__construct();
        $this->user = $this->session->userdata('user');
        $this->load->model('funcionario_model');
        $this->load->model('setor_model');
        $this->load->model('empresa_model');
    }

    public function index() {
        $this->loadIndex("Home", 'home', "pages/home", array(link_tag(base_url('assets/css/home.css'))));
    }

    private function getData($pageTitle, $active) {
        $data['page_title'] = $pageTitle;
        $data['active'] = $active;
        $data['user'] = $this->session->userdata('user');
    }

    private function loadIndex($pageTitle, $active, $view, $extras = null) {
        $data['page_title'] = $pageTitle;
        $data['active'] = $active;
        $data['extras'] = $extras;
        $data['aniversariantes'] = $this->funcionario_model->getAniversariantes();

        $data['numbers'] = array(
            "func_ativos" => $this->funcionario_model->getCount(),
            "setores" => $this->setor_model->getCount(),
            "empresas" => $this->empresa_model->getCount()
        );

        $this->load->view("templates/header", $data);
        $this->load->view("templates/nav", $data);
        $this->load->view($view, $data);
        $this->load->view("templates/footer");
    }

    public function hash() {
        $data['page_title'] = 'Password Hash';
        $data['active'] = '';
        $this->load->view("templates/header", $data);
        $this->load->view("templates/nav", $data);
        $this->load->view("hash", $data);
        $this->load->view("templates/footer");
    }

}
