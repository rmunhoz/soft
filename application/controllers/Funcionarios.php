<?php

class Funcionarios extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('funcionario_model');
        $this->load->model('empresa_model');
        $this->load->model('setor_model');
    }

    public function index() {
        $data['page_title'] = "Funcionários";
        $data['active'] = "funcionarios";
        $data['extras'] = array(
            link_tag(base_url('assets/css/home.css')),
            link_tag('https://cdn.datatables.net/t/bs/dt-1.10.11,se-1.1.2/datatables.min.css'),
            script_tag('https://cdn.datatables.net/t/bs/dt-1.10.11,se-1.1.2/datatables.min.js'),
            script_tag(base_url('assets/js/funcionarios/init.js')),
            script_tag(base_url('assets/js/funcionarios/app.js'))
        );
        $data['funcionarios'] = $this->funcionario_model->getInfosBasicas();
        $result = $this->session->flashdata('result');
        if ($result != FALSE) {
            $data['result'] = $result;
        }
        $this->load->view("templates/header", $data);
        $this->load->view("templates/nav", $data);
        $this->load->view("pages/funcionarios", $data);
        $this->load->view("templates/footer");
    }

    public function delete() {
        $id = $this->input->post('id');
        if ($id == FALSE) {
            $id = $this->input->get('id');
            var_dump($id);
        }
        $json = $this->input->post('json');
        if ($id == FALSE) {
            $ids = $this->input->post('ids');
            if ($ids != FALSE) {
                echo json_encode($this->funcionario_model->batchDelete($ids));
            } else {
                http_response_code(400);
                echo json_encode(false);
            }
        } else {
            if ($json) {
                echo json_encode($this->funcionario_model->delete($id));
            } else {
                if ($this->funcionario_model->delete($id)) {
                    $this->session->set_flashdata('result', '{"result":true, "message":"Funcionário excluído com sucesso!"}');
                    redirect(base_url('/funcionarios/'));
                } else {
                    $this->session->set_flashdata('result', '{"result":false, "message":"Erro ao excluir o funcionário!"}');
                    redirect(base_url("/funcionarios/editar?id=$id"));
                }
            }
        }
    }

    public function inactivate() {
        $id = $this->input->post('id');
        if ($id != FALSE && $id != "") {
            echo json_encode($this->funcionario_model->inactivate($id));
        } else {
            http_response_code(400);
        }
    }

    public function activate() {
        $id = $this->input->post('id');
        if ($id != FALSE && $id != "") {
            echo json_encode($this->funcionario_model->activate($id));
        } else {
            http_response_code(400);
        }
    }

    public function editar() {
        /* @var $model Funcionario_model */
        $id = $this->input->get('id');
        if ($id != "" && $id != false) {
            $data['page_title'] = "Editar Funcionário";
            $data['active'] = "funcionarios";
            $data['funcionario'] = $this->funcionario_model->getDetails($id, FALSE);
            $data['empresas'] = $this->empresa_model->getEmpresas();
            $data['setores'] = $this->setor_model->getSetores();
            $data['extras'] = Array(
                script_tag(base_url('/assets/libs/jquery-ui/jquery-ui.js')),
                link_tag(base_url('/assets/libs/jquery-ui/jquery-ui.css')),
                script_tag(base_url('/assets/js/lib/modernizr.js')),
                script_tag(base_url('/assets/js/lib/mask.js')),
                script_tag(base_url('/assets/js/funcionarios/editar.js'))
            );
            $result = $this->session->flashdata('result');
            if (isset($result)) {
                $data['result'] = $result;
            }
            $this->load->view("templates/header", $data);
            $this->load->view("templates/nav", $data);
            $this->load->view("pages/funcionarios/editar", $data);
            $this->load->view("templates/footer");
        } else {
            http_response_code(400);
        }
    }

    public function detalhes() {
        /* @var $model Funcionario_model */
        $model = $this->funcionario_model;
        $id = $this->input->get('id');
        if ($id != "" && $id != false) {
            $data['page_title'] = "Detalhes do Funcionário";
            $data['active'] = "funcionarios";
            $data['funcionario'] = $model->getDetails($id);
            $this->load->view("templates/header", $data);
            $this->load->view("templates/nav", $data);
            $this->load->view("pages/funcionarios/detalhes", $data);
            $this->load->view("templates/footer");
        } else {
            http_response_code(400);
        }
    }

    public function novo() {
        $data['page_title'] = "Novo Funcionário";
        $data['active'] = "funcionarios";
        $data['empresas'] = $this->empresa_model->getEmpresas();
        $data['setores'] = $this->setor_model->getSetores();
        $data['extras'] = Array(
            script_tag(base_url('/assets/libs/jquery-ui/jquery-ui.js')),
            link_tag(base_url('/assets/libs/jquery-ui/jquery-ui.css')),
            script_tag(base_url('/assets/js/lib/modernizr.js')),
            script_tag(base_url('/assets/js/lib/mask.js')),
            script_tag(base_url('/assets/js/funcionarios/editar.js'))
        );
        $result = $this->session->flashdata('result');
        if (isset($result)) {
            $data['result'] = $result;
        }
        $this->load->view("templates/header", $data);
        $this->load->view("templates/nav", $data);
        $this->load->view("pages/funcionarios/novo", $data);
        $this->load->view("templates/footer");
    }

    public function update() {
        $info = $this->input->post();
        if (count($info) >= 35) {
            if ($this->funcionario_model->update($info)) {
                $this->session->set_flashdata('result', '{"result":true, "message":"Alterações salvas com sucesso!"}');
            } else {
                $this->session->set_flashdata('result', '{"result":false, "message":"Erro ao salvar as alterações!"}');
            }
            redirect(base_url("/funcionarios/editar?id={$info['id']}"));
        } else {
            http_response_code(400);
        }
    }

    public function create() {
        $info = $this->input->post();
        if (count($info) >= 34) {
            $result = $this->funcionario_model->create($info, true);
            if (is_int($result)) {
                $this->session->set_flashdata('result', '{"result":true, "message":"Fucnionário criado com sucesso!"}');
            } else {
                $this->session->set_flashdata('result', '{"result":false, "message":"Erro ao criar o funcionário!"}');
            }
            redirect(base_url("/funcionarios/detalhes?id=$result"));
        } else {
            http_response_code(400);
        }
    }

}
