<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Conntest extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('conntest_model');
    }

    public function index() {
        $result['conntest'] = $this->conntest_model->getConnTestResult();
        $this->load->view('index', $result);
    }

}
