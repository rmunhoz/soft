<?php

class Usuarios extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("usuario_model");
    }

    public function index() {
        $data['page_title'] = "Usuários";
        $data['active'] = "usuarios";
        $data['user'] = $this->session->userdata('user');
        $result = $this->session->flashdata('result');
        if (isset($result)) {
            $data['result'] = $this->session->flashdata('result');
        }
        $data['extras'] = Array(
            link_tag('https://cdn.datatables.net/t/bs/dt-1.10.11,se-1.1.2/datatables.min.css'),
            script_tag('https://cdn.datatables.net/t/bs/dt-1.10.11,se-1.1.2/datatables.min.js'),
            script_tag(base_url("/assets/js/usuarios/app.js")),
            script_tag(base_url("/assets/js/usuarios/init.js"))
        );
        $data['usuarios'] = $this->usuario_model->get_users();
        $this->load->view("templates/header", $data);
        $this->load->view("templates/nav", $data);
        $this->load->view("pages/usuarios/home", $data);
        $this->load->view("templates/footer");
    }

    public function create() {
        $info = $this->input->post();
        if (count($info) >= 5) {
            $result = $this->usuario_model->create($info, true);
            if ($result !== false) {
                $this->session->set_flashdata('result', '{"result":true, "message":"Usuário criado com sucesso!"}');
            } else {
                $this->session->set_flashdata('result', '{"result":false, "message":"Erro ao criar o usuário!"}');
            }
            redirect(base_url("/usuarios"));
        } else {
            http_response_code(400);
        }
    }

    public function delete() {
        if ($this->input->server('REQUEST_METHOD') == 'POST' && empty($this->input->post())) {
            $parsedPost = json_decode(file_get_contents('php://input'), true);
            if (isset($parsedPost)) {
                $_POST = $parsedPost;
            }
        }

        $login = $this->input->post('login');
        if (!isset($login)) {
            http_response_code(400);
        } else {
            if ($login != "" && $login != FALSE) {
                if ($this->usuario_model->delete($login)) {
                    echo(json_encode(true));
                } else {
                    http_response_code(500);
                    echo(json_encode(false));
                }
            } else {
                http_response_code(400);
            }
        }
    }

}
