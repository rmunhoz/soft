<?php

class ResetSenha extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('usuario_model');
        $this->load->model('auth_model');
    }

    public function index() {
        $logado = $this->session->userdata("user");

        if (!(isset($logado)) || $logado == "") {
            $data['page_title'] = "Esqueci Minha Senha";
            $data['extras'] = array(link_tag(base_url('assets/css/login.css')));
            $this->load->view("templates/header", $data);
            $error = $this->session->flashdata('pw_reset_error');
            $message = $this->session->flashdata('pw_reset_message');
            if (isset($error)) {
                $data['error'] = $error;
            }
            if (isset($message)) {
                $data['message'] = $message;
            }
            $this->load->view("passwd/form", $data);
            $this->load->view("templates/footer");
        } else {
            redirect(base_url());
        }
    }

    public function reset() {
        if ($this->input->server('REQUEST_METHOD') == 'POST' && empty($this->input->post())) {
            $parsedPost = json_decode(file_get_contents('php://input'), true);
            if (isset($parsedPost)) {
                $_POST = $parsedPost;
            }
        }
        
        $login = $this->input->post('login');
        $token = $this->input->get('token');
        $senha = $this->input->post('senha');
        $repSenha = $this->input->post('rep_senha');
        $json = $this->input->post('json');
        if (isset($login)) {
            header('Content-Type: text/html; charset=utf-8');
            $result = $this->usuario_model->generate_token($login);
            if (is_array($result)) {
                if ($result['success']) {
                    if (ENVIRONMENT == 'development' && !isset($json)) {
                        $this->session->set_flashdata('pw_reset_message', $result['message'] . "<br>Token: {$result['token']}");
                        redirect(base_url("/resetSenha"));
                    } else {
                        $userEmail = $this->auth_model->getUser($login, "", FALSE);
                        if (isset($userEmail)) {
                            $userEmail = $userEmail->email;
                            $body = $this->load->view('templates/email/passwd', Array("token" => $result['token']), TRUE);
                            $this->load->library('email');
                            $this->email->set_newline("\r\n")
                                    ->from('soft@incolustre.com.br', 'Soft - Não responda')
                                    ->to($userEmail)
                                    ->subject('Nova senha do Soft')
                                    ->message($body);

                            if ($this->email->send()) {
                                if (isset($json)) {
                                    http_response_code(500);
                                    echo json_encode(true);
                                } else {
                                    $this->session->set_flashdata('pw_reset_message', $result['message']);
                                    redirect(base_url("/resetSenha"));
                                }
                            } else {
                                if (isset($json)) {
                                    http_response_code(500);
                                    echo json_encode(false);
                                } else {
                                    $this->session->set_flashdata('pw_reset_error', "Erro ao enviar o link de redefinição. Por gentileza, tente novamente");
                                    redirect(base_url("/resetSenha"));
                                }
                            }
                        } else {
                            $this->session->set_flashdata('pw_reset_error', "Erro ao obter seu email no banco de dados. Por gentileza, tente novamente");
                            redirect(base_url("/resetSenha"));
                        }
                    }
                } else {
                    $this->session->set_flashdata('pw_reset_error', $result['message']);
                    redirect(base_url("/resetSenha"));
                }
            } else {
                $this->session->set_flashdata('pw_reset_error', 'Erro interno. Tente novamente');
                redirect(base_url("/resetSenha"));
            }
        } elseif (isset($token)) {
            $valid = $this->usuario_model->validate_token($token);
            if (is_array($valid)) {
                $this->session->set_flashdata('pw_reset_error', $valid['message']);
                var_dump($valid);
                redirect(base_url("/resetSenha"));
            } else {
                if ($valid === true) {
                    $data['page_title'] = "Redefinir senha";
                    $data['extras'] = array(link_tag(base_url('assets/css/login.css')));
                    $this->load->view("templates/header", $data);
                    $error = $this->session->flashdata('pw_reset_error');
                    $message = $this->session->flashdata('pw_reset_message');
                    if (isset($error)) {
                        $data['error'] = $error;
                    }
                    if (isset($message)) {
                        $data['message'] = $message;
                    }
                    $this->load->view("passwd/reset", $data);
                    $this->load->view("templates/footer");
                }
            }
        } elseif (isset($senha)) {
            if ($senha == $repSenha) {
                $result = $this->usuario_model->passwd($this->input->post('validToken'), $senha);
                if ($result['success'] === true) {
                    $this->session->set_flashdata('login_message', 'Sucesso! Agora você pode logar utilizando a nova senha!');
                    redirect(base_url("/login"));
                } else {
                    $this->session->set_flashdata('pw_reset_error', 'Erro ao redefinir a senha; por favor, tente novamente');
                    redirect(base_url('/resetSenha'));
                }
            } else {
                $this->session->set_flashdata('pw_reset_error', 'As senhas não coincidem; tente novamente');
                redirect(base_url("/resetSenha/reset?token={$this->input->post('validToken')}"));
            }
        } else {
            $this->session->set_flashdata('pw_reset_error', 'Informe seu login!');
            //redirect(base_url("/resetSenha"));
            var_dump($this->input->post());
            var_dump($this->input->get());
        }
    }

}
