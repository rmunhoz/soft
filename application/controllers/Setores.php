<?php

class Setores extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('setor_model');
    }

    public function index() {
        $data['page_title'] = "Setores";
        $data['active'] = 'setores';
        $data['setores'] = $this->setor_model->getSetores();
        $data['extras'] = array(
            script_tag(base_url('assets/js/setores/app.js')),
            link_tag(base_url('assets/css/empresas.css'))
        );
        $this->load->view("templates/header", $data);
        $this->load->view("templates/nav", $data);
        $this->load->view("pages/setores", $data);
        $this->load->view("templates/footer");
    }

    public function delete($id) {
        if (!isset($id)) {
            http_response_code(400);
        } else {
            echo json_encode($this->setor_model->delete($id));
        }
    }

    public function insert() {
        $nome = $this->input->get('nome');
        if (isset($nome) && $nome != "" && $nome != FALSE) {
            echo(json_encode($this->setor_model->insert($nome)));
        } else {
            http_response_code(400);
            echo($nome);
        }
    }

    public function update() {
        $id = $this->input->post('id');
        $nome = $this->input->post('nome');
        if (isset($nome) && isset($id) && $nome != false && $id != false) {
            echo json_encode($this->setor_model->update($id, $nome));
        } else {
            http_response_code(400);
        }
    }

}
