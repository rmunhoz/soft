<div class="container">
    <? if (isset($error)): ?>
        <div class="col-md-3 error">
            <div class="alert alert-danger" role="alert"><strong>Erro! </strong> <?= $error ?></div>
        </div>
    <? endif; ?>
    <? if (isset($message)): ?>
        <div class="col-md-3 message">
            <div class="alert alert-info" role="alert"><?= $message ?></div>
        </div>
    <? endif; ?>
    <div class="well well-sm col-md-3">
        <form class="form-signin" action="<?= base_url('login/doLogin?XDEBUG_SESSION_START=netbeans-xdebug') ?>" method="POST">
            <h2 class="form-signin-heading">Soft - Login</h2>
            <label for="login" class="sr-only">Login</label>
            <input id="login" class="form-control" placeholder="Login" required="" autofocus="" type="text" name="login">
            <br>
            <label for="senha" class="sr-only">Senha</label>
            <input id="senha" class="form-control" placeholder="Senha" required="" type="password" name="senha">
            <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button><br>
            <center><a href="<?= base_url('/ResetSenha') ?>">Esqueci a Senha</a></center>
        </form>
        <? if (ENVIRONMENT == 'development'): ?>
            <div class="alert alert-info text-center" role="alert">Ambiente de desenvolvimento</div>
        <? endif; ?>
    </div>
</div>