<html>
    <head>
        <style>
            h1 {
                margin: 5px;
                color: grey;
            }
            hr {
                color: lightgrey;
                border-color: lightgrey;
                border-style:solid;
            }
            p{
                color: #454545;
            }
            a{
                text-decoration: none;
            }
            a:visited{
                color: red;
                text-decoration: line-through;
            }
        </style>
    <head>
    <body>
        <h1>Soft</h1>
        <hr>
        <p>
            Você está recebendo este email pois solicitou uma nova senha de acesso ao Soft<br>
            Clique no link para redefini-la, ele é vélido por uma hora: <a href="http://incolustre.com.br/soft/ResetSenha/reset?token=<?= $token ?>">http://incolustre.com.br/soft/resetsenha/reset?token=<?= $token ?></a><br>
        </p>

        <p>
            Caso você não tenha solicitado a alteração de senha, apenas ignore este email.
        </p>
    </body>
</html>