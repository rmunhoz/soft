<? $user = $this->session->userdata('user'); ?>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?= base_url() ?>">Soft</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <?
                $pages = array(
                    $this->page->cPage("Home", '', 'home'),
                    $this->page->cPage("Empresas", "empresas", 'empresas'),
                    $this->page->cPage("Setores", "setores", 'setores'),
                    $this->page->cPage("Funcionarios", "funcionarios", 'funcionarios')
                );
                foreach ($pages as $page):
                    ?>
                    <?
                    if (isset($active) && $page->name != $active) {
                        echo("<li>" . anchor(base_url($page->url), $page->title) . "</li>");
                    } else {
                        echo('<li class="active">' . anchor(base_url($page->url), $page->title) . '</li>');
                    }
                    ?> 
                <? endforeach; ?>
                <?
                if ($user->admin) {
                    echo("<li>" . anchor(base_url("usuarios"), "Usuários") . "</li>");
                }
                ?>
            </ul>
            <ul class="navbar-right nav navbar-nav">
                <li class="dropdown">
                    <? if (isset($user)): ?>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?= $user->nome ?> <span class="caret"></span></a>
                    <? endif; ?>
                    <ul class="dropdown-menu">
                        <?
                        $pages = array(
                            'profile' => 'Perfil',
                            'configs' => 'Configurações'
                                )
                        ?>
                        <? foreach ($pages as $page => $title): ?>
                            <li><?= anchor(base_url('user/' . $page), $title) ?></li>
                        <? endforeach; ?>
                        <li role="separator" class="divider"></li>
                        <li><?= anchor(base_url("logout"), "Logout") ?></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container content">