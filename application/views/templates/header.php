<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Soft - <?= $page_title ?></title>

        <!-- Main CSS -->
        <?= link_tag(base_url('assets/css/main.css')) ?>

        <!-- jQuery-->
        <?= script_tag(base_url('assets/js/lib/jquery.js')) ?>

        <!-- Bootstrap -->
        <?= link_tag(base_url('assets/css/lib/bootstrap.css')) ?>
        <?= script_tag(base_url('assets/js/lib/bootstrap.js')) ?>

        <!-- Angular.JS -->
        <?= script_tag(base_url('assets/js/lib/angular.js')) ?>

        <!-- Font Awesome -->
        <?= link_tag(base_url('/assets/css/lib/fa.css')) ?>

        <?
        if (isset($extras)) {
            echo("<!-- Page Extras -->\n");
            foreach ($extras as $extra) {
                echo $extra . "\n";
            }
        }
        ?>
    </head>
    <body>
        <div id="content">