<? $senha = $this->input->post('senha'); ?>
<div class="container-fluid">
    <div class="page-header">
        <h1>Gerar hash de senha</h1>
    </div>
    <div class="row">
        <form class="form-inline" action="" method="POST">
            <label for="senha">Senha:</label>
            <input type="text" name="senha" class="form-control" />
            <button type="submit" value="Criptografar" class="btn btn-info">Criptografar</button>
        </form>
    </div>
</div>

<? if (isset($senha)): ?>
    <label for="cypted">Hash:</label>
    <div class="encrypted"><?= password_hash($senha, PASSWORD_BCRYPT) ?></div>
<? endif; ?>