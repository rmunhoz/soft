<div class="container">
    <? if (isset($error)): ?>
        <div class="col-md-3 error">
            <div class="alert alert-danger" role="alert"><strong>Erro! </strong> <?= $error ?></div>
        </div>
    <? endif; ?>
    <? if (isset($message)): ?>
        <div class="col-md-3 message">
            <div class="alert alert-info" role="alert"><?= $message ?></div>
        </div>
    <? endif; ?>
    <div class="well well-sm col-md-3">
        <form class="form-signin" action="<?= base_url('resetSenha/reset') ?>" method="POST">
            <h2 class="form-signin-heading">Informe seu Login</h2>
            <label for="login" class="sr-only">Login</label>
            <input id="login" class="form-control" placeholder="Login" required autofocus="" type="text" name="login">
            <br>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Redefinir Senha</button><br>
            <center><a href="<?= base_url('/login') ?>">Voltar para o Login</a></center>
        </form>
        <? if (ENVIRONMENT == 'development'): ?>
            <div class="alert alert-info text-center" role="alert">Ambiente de desenvolvimento</div>
        <? endif; ?>
    </div>
</div>