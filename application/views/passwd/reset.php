<div class="container">
    <? if (isset($error)): ?>
        <div class="col-md-3 error">
            <div class="alert alert-danger" role="alert"><strong>Erro! </strong> <?= $error ?></div>
        </div>
    <? endif; ?>

    <div class="well well-sm col-md-3">
        <form class="form-signin" action="<?= base_url('resetSenha/reset') ?>" method="POST">
            <input type="hidden" value="<?= $this->input->get('token'); ?>" name="validToken"/>
            <h2 class="form-signin-heading">Escolha uma nova senha</h2>
            <label for="senha" class="sr-only">Senha</label>
            <input id="senha" class="form-control" placeholder="Senha" required autofocus="" type="password" name="senha">
            <br>
            <label for="rep_senha" class="sr-only">Repita a senha</label>
            <input id="rep_senha" class="form-control" placeholder="Repita a senha" required type="password" name="rep_senha">
            <br>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Redefinir Senha</button>
        </form>
        <? if (ENVIRONMENT == 'development'): ?>
            <div class="alert alert-info text-center" role="alert">Ambiente de desenvolvimento</div>
        <? endif; ?>
    </div>
</div>