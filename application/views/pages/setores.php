<div class="container-fluid" ng-app="Setores">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>
                    Setores <small>Gerenciar setores cadastrados no <strong>Soft</strong></small>
                </h1>
            </div>
        </div>
    </div>
    <div class="row" ng-controller="NavController as nc">
        <div class="col-md-3">
            <div class="list-group">
                <a class="list-group-item" ng-class="{'active': nc.isActive('visualizar')}" ng-click="nc.setActive('visualizar')">Visualizar Setores</a>
                <a class="list-group-item" ng-class="{'active': nc.isActive('cadastrar')}" ng-click="nc.setActive('cadastrar')">Cadastrar Setores</a>
            </div>
        </div>

        <div ng-show="nc.isActive('visualizar')">
            <?
            $count = count($setores);
            if ($count > 1) {
                $text = $count . " setores";
            } else if ($count > 0) {
                $text = "1 setor";
            }
            ?>
            <? if ($count > 0): ?>
                <div class="col-md-9">
                    <div class="panel panel-default" ng-controller="ListController as lc">
                        <div class="panel-heading">
                            <h1 class="panel-title">
                                Setores Cadastrados
                                <button type="button" class="btn btn-xs btn-danger pull-right" title="Excluir setores selecionados" ng-click="lc.deleteSelected()" ng-show="lc.hasSelection()">
                                    <i class="fa fa-trash"></i>&nbsp;Excluir setores selecionados
                                </button>
                            </h1>
                        </div>
                        <div class="panel-body">
                            <div class="list-group" style="margin: 0">
                                <? foreach ($setores as $setor): ?>
                                    <div class="list-group-item" item-id="<?= $setor['id'] ?>" ng-show="lc.shouldBeShown(<?= $setor['id'] ?>)">
                                        <div>
                                            <input type="text" value="<?= htmlspecialchars($setor['nome']) ?>" class="col-md-2" ng-model="lc.nomeEdit" ng-show="lc.editando(<?= $setor['id'] ?>)" /> &nbsp;
                                            <element ng-hide="lc.editando(<?= $setor['id'] ?>)" ><?= htmlspecialchars($setor['nome']) ?></element>
                                            <div class="pull-right">
                                                
                                                <!-- Editando -->
                                                <div class="label label-default" ng-show="lc.finalizando(<?= $setor['id'] ?>)"><i class="fa fa-refresh fa-spin"></i></i></div>
                                                <button type="button" class="btn btn-xs btn-danger" title="Cancelar" ng-click="lc.cancelarEdicao()" ng-disabled="lc.finalizando(<?= $setor['id'] ?>)" ng-show="lc.editando(<?= $setor['id'] ?>)">
                                                    <i class="fa fa-remove"></i>
                                                </button>
                                                <button type="button" class="btn btn-xs btn-success" title="Concluir" ng-click="lc.finalizar()" ng-disabled="lc.finalizando(<?= $setor['id'] ?>)" ng-show="lc.editando(<?= $setor['id'] ?>)">
                                                    <i class="fa fa-check"></i>
                                                </button>
                                                
                                                <!-- Seleção -->
                                                <button type="button" class="btn btn-xs btn-default" title="Marcar <?= htmlspecialchars($setor['nome']) ?>" ng-click="lc.select(<?= $setor['id'] ?>)" ng-hide="lc.selected(<?= $setor['id'] ?>) || lc.editando(<?= $setor['id'] ?>)">
                                                    <i class="fa fa-square-o"></i>
                                                </button>
                                                <button type="button" class="btn btn-xs btn-default" title="Desmarcar <?= htmlspecialchars($setor['nome']) ?>" ng-click="lc.unselect(<?= $setor['id'] ?>)" ng-show="lc.selected(<?= $setor['id'] ?>) && !lc.editando(<?= $setor['id'] ?>)">
                                                    <i class="fa fa-check-square-o"></i>
                                                </button>
                                                
                                                <!-- Default -->
                                                <button type="button" class="btn btn-xs btn-primary" title="Editar <?= htmlspecialchars($setor['nome']) ?>" ng-click="lc.editar(<?= $setor['id'] ?>, '<?= htmlspecialchars($setor['nome']) ?>')" ng-hide="lc.editando(<?= $setor['id'] ?>)">
                                                    <i class="fa fa-pencil"></i>
                                                </button>
                                                <button type="button" class="btn btn-xs btn-danger" title="Excluir <?= htmlspecialchars($setor['nome']) ?>" ng-click="lc.deleteSetor(<?= $setor['id'] ?>)" ng-hide="lc.editando(<?= $setor['id'] ?>)">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                                
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </div>
                        <div class="panel-footer">
                            Total: <?= $text ?>
                        </div>
                    </div>
                </div>
            <? else: ?>
                <div class="col-md-9">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center">Nenhum setor cadastrado</h3>
                        </div>
                        <div class="panel-body text-center">
                            Você pode cadastrar setores através do menu à esquerda.
                        </div>
                    </div>
                </div>
            <? endif; ?>
        </div>

        <div ng-show="nc.isActive('cadastrar')">
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Cadastrar Setores</h3>
                    </div>
                    <div class="panel-body" ng-controller="CadastroController as cc">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="nomeSetor" placeholder="Nome do setor" ng-model="cc.nome">
                                        <span class="input-group-btn"><button class="btn btn-primary" ng-click="cc.addSetor(cc.nome)">Adicionar</button></span>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="list-group" style="margin: 0">
                            <div class="list-group-item active text-center">Setores Adicionados</div>
                            <div class="list-group-item" ng-repeat="setor in cc.paraCadastrar">
                                {{setor}}
                                <div class="pull-right">
                                    <div class="label label-default" ng-show="cc.processing(setor)"><i class="fa fa-refresh fa-spin"></i></i></div>
                                    <button class="btn btn-danger btn-xs" ng-click="cc.removeSetor(setor)" ng-show="cc.deletable(setor)">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <div class="label label-success" ng-show="cc.success(setor)">
                                        <i class="fa fa-check"></i>&nbsp;
                                        Cadastrado com sucesso
                                    </div>
                                    <div class="label label-danger" ng-show="cc.error(setor)">
                                        <i class="fa fa-exclamation-triangle"></i>&nbsp;
                                        Erro ao cadastrar
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-block btn-primary" ng-show="cc.canSave()" ng-click="cc.finaliza()" ng-disabled="cc.processing()">Salvar</button>
                            <button class="btn btn-block btn-success" ng-show="!cc.canSave() && cc.paraCadastrar.length > 0" ng-click="cc.limpa()" ng-disabled="cc.processing()">Limpar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>