<div class="container" ng-app="Usuarios" ng-controller="PageController as pc">
    <div class="page-header">
        <?
        if (isset($result)) {
            $result = json_decode($result);
        }
        ?>
        <? if (isset($result)): ?>
            <? if ($result->result): ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?= $result->message ?>
                </div>
            <? else: ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?= $result->message ?>
                </div>
            <? endif; ?>
        <? endif; ?>
        <h1>Usuários<small class="pull-right">Em construção</small></h1>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="list-group">
                <div class="list-group-item" ng-click="pc.select('visualizar')" ng-class="{active: pc.isSelected('visualizar')}">Visualizar Usuários</div>
                <div class="list-group-item" ng-click="pc.select('novo')" ng-class="{active: pc.isSelected('novo')}">Criar Usuário</div>
            </div>
        </div>
        <div class="col-md-9" ng-show="pc.isSelected('visualizar')">
            <table id="tabUsuarios" class="display" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <?
                        $campos = Array("Nome" => 'nome', "Login" => 'login', "Email" => 'email', "Admin" => 'admin');
                        ?>
                        <? foreach ($campos as $campo => $dbkey): ?>
                            <th class="text-center"><?= $campo ?></th>
                        <? endforeach; ?>
                        <th class="text-center">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <? foreach ($usuarios as $usuario): ?>
                        <tr class="text-center">
                            <? foreach ($campos as $campo => $dbkey): ?>
                                <td><?= htmlspecialchars($usuario[$dbkey]) ?></td>
                            <? endforeach; ?>
                            <td class="text-center">
                                <span ng-show="!pc.isProcessing('<?= $usuario['login'] ?>')">
                                    <button class="btn btn-xs btn-warning" title="Redefinir Senha" ng-click="pc.resetSenha('<?= $usuario['login'] ?>'); $event.stopPropagation();">
                                        <i class="fa fa-key"></i>
                                    </button>
                                    <button  class="btn btn-xs btn-primary" title="Editar" ng-click="$event.stopPropagation()">
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                    <button class="btn btn-xs btn-danger" title="Excluir" ng-click="pc.deleteUser('<?= $usuario['login'] ?>')">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </span>
                                <div class="label label-default" ng-show="pc.isProcessing('<?= $usuario['login'] ?>')"><i class="fa fa-refresh fa-spin"></i></i></div>
                            </td>
                        </tr>
                    <? endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-9" ng-show="pc.isSelected('novo')">
            <form class="inline-form" action="<?= base_url("/usuarios/create") ?>" method="POST">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nome">Nome:</label>
                            <input class="form-control" type="text" name="nome" id="login" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="sobrenome">Sobrenome:</label>
                            <input class="form-control" type="text" name="sobrenome" id="sobrenome" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input class="form-control" type="email" name="email" id="email" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="login">Login:</label>
                            <input class="form-control" type="text" name="login" id="login" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="senha">Senha:</label>
                            <input class="form-control" type="password" name="senha" id="senha" />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="senha">Admin:</label>
                            <select class="form-control" id="admin" name="admin">
                                <option value="0" selected>Não</option>
                                <option value="1" selected>Sim</option>
                            </select>
                        </div>
                    </div>
                </div>
                <input class="btn btn-block btn-success" type="submit" value="Cadastrar" />
            </form>
        </div>
    </div>
</div>