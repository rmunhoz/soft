<div class="container-fluid" ng-app="Funcionarios">
    <div class="page-header">
        <?
        if (isset($result)) {
            $result = json_decode($result);
        }
        ?>
        <? if (isset($result)): ?>
            <? if ($result->result): ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?= $result->message ?>
                </div>
            <? else: ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?= $result->message ?>
                </div>
            <? endif; ?>
        <? endif; ?>
        <h1>Funcionários <small>Gerenciar funcionários cadastrados no <strong>Soft</strong></small></h1>
    </div>
    <div class="col-md-3">
        <div class="list-group">
            <div class="list-group-item active">Visualizar Funcionários</div>
            <a href="<?= base_url('/funcionarios/novo') ?>" class="list-group-item">Novo Funcionário</a>
        </div>
    </div>
    <div class="col-md-9" ng-controller="TableController as tc">
        <div class="well well-sm" ng-show="tc.hasSelection()">
            <strong>Ações em lote: </strong>
            <button class="btn btn-xs btn-success" title="Ativar" ng-click="tc.activateSelected(); $event.stopPropagation()">
                <i class="fa fa-check"></i>
            </button>
            <button class="btn btn-xs btn-warning" title="Desativar" ng-click="tc.inactivateSelected(); $event.stopPropagation()">
                <i class="fa fa-exclamation-triangle"></i>
            </button>
            <button class="btn btn-xs btn-danger" title="Excluir" ng-click="tc.deleteSelected(); $event.stopPropagation()">
                <i class="fa fa-trash"></i>
            </button>
        </div>
        <table id="tabFuncionarios" class="display" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center">Nome</th>
                    <th class="text-center">Cargo</th>
                    <th class="text-center">Empresa</th>
                    <th class="text-center">Setor</th>
                    <th class="text-center">Ações</th>
                </tr>
            </thead>
            <tbody>
                <? foreach ($funcionarios as $funcionario): ?>
                    <tr class="text-center <? if ($funcionario['ativo'] != 1): ?>warning<? endif; ?>" reg-id="<?= $funcionario['id'] ?>" reg-active="<?= $funcionario['ativo'] ?>">
                        <td><?= htmlspecialchars($funcionario['id']) ?></td>
                        <td><?= htmlspecialchars($funcionario['nome']) ?> <?= htmlspecialchars($funcionario['sobrenome']) ?></td>
                        <td><?= htmlspecialchars($funcionario['funcao']) ?></td>
                        <td><?= htmlspecialchars($funcionario['empresa']) ?></td>
                        <td><?= htmlspecialchars($funcionario['setor']) ?></td>
                        <td class="text-center">
                            <a href="<?= base_url("/funcionarios/detalhes?id={$funcionario['id']}") ?>" class="btn btn-xs btn-info" title="Detalhes" ng-disabled="!tc.hasActions(<?= $funcionario['id'] ?>)" ng-click="$event.stopPropagation()">
                                <i class="fa fa-info-circle"></i>
                            </a>
                            <a href="<?= base_url("/funcionarios/editar?id={$funcionario['id']}") ?>" class="btn btn-xs btn-primary" title="Editar" ng-disabled="!tc.hasActions(<?= $funcionario['id'] ?>)" ng-click="$event.stopPropagation()">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <? if ($funcionario['ativo'] == 0): ?>
                                <button class="btn btn-xs btn-success" title="Ativar" ng-disabled="!tc.hasActions(<?= $funcionario['id'] ?>)" ng-click="tc.activate(<?= $funcionario['id'] ?>); $event.stopPropagation()">
                                    <i class="fa fa-check"></i>
                                </button>
                            <? else: ?>
                                <button class="btn btn-xs btn-warning" title="Desativar" ng-disabled="!tc.hasActions(<?= $funcionario['id'] ?>)" ng-click="tc.inactivate(<?= $funcionario['id'] ?>); $event.stopPropagation()">
                                    <i class="fa fa-exclamation-triangle"></i>
                                </button>
                            <? endif; ?>
                            <button class="btn btn-xs btn-danger" title="Excluir" ng-show="tc.hasActions(<?= $funcionario['id'] ?>)" ng-click="tc.deleteOne(<?= $funcionario['id'] ?>); $event.stopPropagation()">
                                <i class="fa fa-trash"></i>
                            </button>
                            <div class="label label-default" ng-show="!tc.hasActions(<?= $funcionario['id'] ?>)"><i class="fa fa-refresh fa-spin"></i></i></div>
                        </td>
                    </tr>
                <? endforeach; ?>
            </tbody>
        </table>
    </div>
</div>