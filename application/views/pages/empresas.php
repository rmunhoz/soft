<div class="container-fluid" ng-app="Empresas">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>
                    Empresas <small>Gerenciar empresas cadastradas no <strong>Soft</strong></small>
                </h1>
            </div>
        </div>
    </div>
    <div class="row" ng-controller="NavController as nc">
        <div class="col-md-3">
            <div class="list-group">
                <a class="list-group-item" ng-class="{'active': nc.isActive('visualizar')}" ng-click="nc.setActive('visualizar')">Visualizar Empresas</a>
                <a class="list-group-item" ng-class="{'active': nc.isActive('cadastrar')}" ng-click="nc.setActive('cadastrar')">Cadastrar Empresas</a>
            </div>
        </div>

        <div ng-show="nc.isActive('visualizar')">
            <?
            $count = count($empresas);
            if ($count > 1) {
                $text = $count . " empresas";
            } else if ($count > 0) {
                $text = "1 empresa";
            }
            ?>
            <? if ($count > 0): ?>
                <div class="col-md-9">
                    <div class="panel panel-default" ng-controller="ListController as lc">
                        <div class="panel-heading">
                            <h1 class="panel-title">
                                Empresas Cadastradas
                                <button type="button" class="btn btn-xs btn-danger pull-right" title="Excluir empresas selecionadas" ng-click="lc.deleteSelected()" ng-show="lc.hasSelection()">
                                    <i class="fa fa-trash"></i>&nbsp;Excluir empresas selecionadas
                                </button>
                            </h1>
                        </div>
                        <div class="panel-body">
                            <div class="list-group" style="margin: 0">
                                <? foreach ($empresas as $empresa): ?>
                                    <div class="list-group-item" item-id="<?= $empresa['id'] ?>" ng-show="lc.shouldBeShown(<?= $empresa['id'] ?>)">
                                        <div>
                                            <input type="text" value="<?= htmlspecialchars($empresa['nome']) ?>" class="col-md-2" ng-model="lc.nomeEdit" ng-show="lc.editando(<?= $empresa['id'] ?>)" /> &nbsp;
                                            <element ng-hide="lc.editando(<?= $empresa['id'] ?>)" ><?= htmlspecialchars($empresa['nome']) ?></element>
                                            <div class="pull-right">

                                                <!-- Editando -->
                                                <div class="label label-default" ng-show="lc.finalizando(<?= $empresa['id'] ?>)"><i class="fa fa-refresh fa-spin"></i></i></div>
                                                <button type="button" class="btn btn-xs btn-danger" title="Cancelar" ng-click="lc.cancelarEdicao()" ng-disabled="lc.finalizando(<?= $empresa['id'] ?>)" ng-show="lc.editando(<?= $empresa['id'] ?>)">
                                                    <i class="fa fa-remove"></i>
                                                </button>
                                                <button type="button" class="btn btn-xs btn-success" title="Concluir" ng-click="lc.finalizar()" ng-disabled="lc.finalizando(<?= $empresa['id'] ?>)" ng-show="lc.editando(<?= $empresa['id'] ?>)">
                                                    <i class="fa fa-check"></i>
                                                </button>

                                                <!-- Seleção -->
                                                <button type="button" class="btn btn-xs btn-default" title="Marcar <?= htmlspecialchars($empresa['nome']) ?>" ng-click="lc.select(<?= $empresa['id'] ?>)" ng-hide="lc.selected(<?= $empresa['id'] ?>) || lc.editando(<?= $empresa['id'] ?>)">
                                                    <i class="fa fa-square-o"></i>
                                                </button>
                                                <button type="button" class="btn btn-xs btn-default" title="Desmarcar <?= htmlspecialchars($empresa['nome']) ?>" ng-click="lc.unselect(<?= $empresa['id'] ?>)" ng-show="lc.selected(<?= $empresa['id'] ?>) && !lc.editando(<?= $empresa['id'] ?>)">
                                                    <i class="fa fa-check-square-o"></i>
                                                </button>

                                                <!-- Default -->
                                                <button type="button" class="btn btn-xs btn-primary" title="Editar <?= htmlspecialchars($empresa['nome']) ?>" ng-click="lc.editar(<?= $empresa['id'] ?>, '<?= htmlspecialchars($empresa['nome']) ?>')" ng-hide="lc.editando(<?= $empresa['id'] ?>)">
                                                    <i class="fa fa-pencil"></i>
                                                </button>
                                                <button type="button" class="btn btn-xs btn-danger" title="Excluir <?= htmlspecialchars($empresa['nome']) ?>" ng-click="lc.deleteEmpresa(<?= $empresa['id'] ?>)" ng-hide="lc.editando(<?= $empresa['id'] ?>)">
                                                    <i class="fa fa-trash"></i>
                                                </button>

                                            </div>
                                        </div>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </div>
                        <div class="panel-footer">
                            Total: <?= $text ?>
                        </div>
                    </div>
                </div>
            <? else: ?>
                <div class="col-md-9">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center">Nenhuma empresa cadastrada</h3>
                        </div>
                        <div class="panel-body text-center">
                            Você pode cadastrar empresas através do menu à esquerda.
                        </div>
                    </div>
                </div>
            <? endif; ?>
        </div>

        <div ng-show="nc.isActive('cadastrar')">
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Cadastrar Empresas</h3>
                    </div>
                    <div class="panel-body" ng-controller="CadastroController as cc">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="nomeEmpresa" placeholder="Nome da empresa" ng-model="cc.nome">
                                        <span class="input-group-btn"><button class="btn btn-primary" ng-click="cc.addEmpresa(cc.nome)">Adicionar</button></span>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="list-group" style="margin: 0">
                            <div class="list-group-item active text-center">Empresas Adicionadas</div>
                            <div class="list-group-item" ng-repeat="empresa in cc.paraCadastrar">
                                {{empresa}}
                                <div class="pull-right">
                                    <div class="label label-default" ng-show="cc.processing(empresa)"><i class="fa fa-refresh fa-spin"></i></i></div>
                                    <button class="btn btn-danger btn-xs" ng-click="cc.removeEmpresa(empresa)" ng-show="cc.deletable(empresa)">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <div class="label label-success" ng-show="cc.success(empresa)">
                                        <i class="fa fa-check"></i>&nbsp;
                                        Cadastrado com sucesso
                                    </div>
                                    <div class="label label-danger" ng-show="cc.error(empresa)">
                                        <i class="fa fa-exclamation-triangle"></i>&nbsp;
                                        Erro ao cadastrar
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-block btn-primary" ng-show="cc.canSave()" ng-click="cc.finaliza()" ng-disabled="cc.processing()">Salvar</button>
                            <button class="btn btn-block btn-success" ng-show="!cc.canSave() && cc.paraCadastrar.length > 0" ng-click="cc.limpa()" ng-disabled="cc.processing()">Limpar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>