<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>
                    Home <small>Visão geral do sistema</small>
                </h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Empresas Cadastradas
                    </h3>
                </div>
                <div class="panel-body">
                    <h1><?= $numbers['empresas'] ?></h1>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Setores Cadastrados
                    </h3>
                </div>
                <div class="panel-body">
                    <h1><?= $numbers['setores'] ?></h1>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Funcionários Ativos
                    </h3>
                </div>
                <div class="panel-body">
                    <h1><?= $numbers['func_ativos'] ?></h1>
                </div>
            </div>
        </div>
    </div>
    <? if (count($aniversariantes) > 0): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="list-group">
                    <div class="list-group-item active">
                        <h4 class="list-group-item-heading">
                            Aniversariantes do Mês
                        </h4>
                    </div>
                    <? foreach ($aniversariantes as $aniversariante): ?>
                        <a class="list-group-item"><span class="badge">Faz <?= $aniversariante['idade'] ?> Anos</span><span class="badge">Dia <?= $aniversariante['dia_nascimento'] ?></span><strong><?= $aniversariante['nome'] ?> <?= $aniversariante['sobrenome'] ?></strong> - <?= $aniversariante['funcao'] ?> - <?= $aniversariante['setor'] ?></a>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    <? endif; ?>
    