<?

function format_date($date) {
    $date = strtotime($date);
    return date('d/m/Y', $date);
}

$funcionario = $funcionario[0]
?>
<div class="container" ng-app="editar" ng-controller="BtnController as bc">
    <? if ($funcionario != null): ?>
        <div class="page-header">
            <?
            if (isset($result)) {
                $result = json_decode($result);
            }
            ?>
            <? if (isset($result)): ?>
                <? if ($result->result): ?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?= $result->message ?>
                    </div>
                <? else: ?>
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?= $result->message ?>
                    </div>
                <? endif; ?>
            <? endif; ?>
            <h1>
                <small><i class="fa fa-pencil"></i> Editar Funcionário </small>
                <?= $funcionario['nome'] ?> <?= $funcionario['sobrenome'] ?>
                <? if ($funcionario['ativo'] != 1): ?>
                    <div class="badge">Inativo</div>
                <? endif; ?>
                <div class="pull-right">
                    <a href="<?= base_url("funcionarios/detalhes?id={$funcionario['id']}") ?>" class="btn btn-primary">
                        <i class="fa fa-info-circle" ng-hide="bc.isTogglingActiveStatus"></i> Voltar para detalhes
                    </a>
                    <? if ($funcionario['ativo'] == 1): ?>
                        <div class="btn btn-warning" ng-disabled="bc.isTogglingActiveStatus" ng-click="bc.inactivate(<?= $funcionario['id'] ?>)">
                            <i class="fa fa-exclamation-triangle" ng-hide="bc.isTogglingActiveStatus"></i><i class="fa fa-refresh fa-spin" ng-show="bc.isTogglingActiveStatus"></i> Marcar como inativo
                        </div>
                    <? else: ?>
                        <div class="btn btn-success" ng-disabled="bc.isTogglingActiveStatus" ng-click="bc.activate(<?= $funcionario['id'] ?>)">
                            <i class="fa fa-check" ng-hide="bc.isTogglingActiveStatus"></i><i class="fa fa-refresh fa-spin" ng-show="bc.isTogglingActiveStatus"></i> Marcar como ativo
                        </div>
                    <? endif; ?>

                    <a href="<?= base_url("/funcionarios/delete?id={$funcionario['id']}") ?>" class="btn btn-danger" ng-disabled="bc.isTogglingActiveStatus" >
                        <i class="fa fa-trash"></i> Excluir
                    </a>
                </div>
            </h1>
        </div>
        <?= $this->input->get('dump') == 1 ? var_dump($funcionario) : '' ?>
        <div class="col-md-12">
            <form class="form" action="<?= base_url('funcionarios/update') ?>" method="POST">
                <input type="hidden" name='id' value="<?= $funcionario['id'] ?>">
                <div class="row">
                    <div class="col-md-2">
                        <!--                    <div class="row">-->
                        <img src="http://placehold.it/150x200" width="100%">
                        <!--</div>-->
                        &nbsp;
                        <!--<div class="row">-->
                        <input type="file" class="file">
                        <div class="btn btn-primary btn-block">Escolher foto</div>
                        <!--</div>-->
                        <br>
                    </div>
                    <div class="col-md-10">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Informações Básicas
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <label for="name">Nome:</label>
                                        <input class="form-control" type="text" name="nome" id="nome" value="<?= $funcionario['nome'] ?>"/>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="sobrenome">Sobrenome:</label>
                                        <input class="form-control" type="text" name="sobrenome" id="sobrenome" value="<?= $funcionario['sobrenome'] ?>"/>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="data_nascimento">Data de Nascimento</label>
                                        <input class="form-control" type="date" name="data_nascimento" id="data_nascimento" value="<?= format_date($funcionario['data_nascimento']) ?>"/>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="sexo">Sexo:</label>
                                        <select class="form-control" name="sexo" id="sexo">
                                            <option<?= $funcionario['sexo'] == 'Masculino' ? ' selected' : '' ?>>Masculino</option>
                                            <option<?= $funcionario['sexo'] == 'Feminino' ? ' selected' : '' ?>>Feminino</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 form-group">
                                        <label for="local_de_nascimento">Cidade Natal:</label>
                                        <input class="form-control" type="text" id="local_de_nascimento" name="local_de_nascimento" value="<?= $funcionario['local_de_nascimento'] ?>">
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="telefone">Telefone:</label>
                                        <input type="text" class="form-control" name="telefone" id="telefone" value="<?= $funcionario['telefone'] ?>" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="celular">Celular:</label>
                                        <input type="text" class="form-control" name="celular" id="celular" value="<?= $funcionario['celular'] ?>" />
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label for="email">Email:</label>
                                        <input type="text" class="form-control" name="email" id="email" value="<?= $funcionario['email'] ?>" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Endereço
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-2 form-group">
                                        <label for="cep">CEP:</label>
                                        <input class="form-control" type="text" id="cep" name="cep" value="<?= $funcionario['cep'] ?>">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="endereco">Rua:</label>
                                        <input class="form-control" type="text" name="endereco" id="endereco" value="<?= $funcionario['endereco'] ?>"/>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="numero">Número:</label>
                                        <input class="form-control" type="text" id="numero" name="numero" value="<?= $funcionario['numero'] ?>">
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="municipio">Município:</label>
                                        <input class="form-control" type="text" id="municipio" name="municipio" value="<?= $funcionario['municipio'] ?>">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <label for="bairro">Bairro:</label>
                                        <input class="form-control" type="text" id="bairro" name="bairro" value="<?= $funcionario['bairro'] ?>">
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <label for="complemento">Complemento:</label>
                                        <input class="form-control" type="text" id="complemento" name="complemento" value="<?= $funcionario['complemento'] ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Dados Pessoais
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="mae">Nome da Mãe:</label>
                                        <input class="form-control" type="text" name="mae" id="mae" value="<?= $funcionario['mae'] ?>" />
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="pai">Nome do Pai:</label>
                                        <input class="form-control" type="text" name="pai" id="pai" value="<?= $funcionario['pai'] ?>" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <label for="escolaridade">Escolaridade:</label>
                                        <select class="form-control" name="escolaridade" id="escolaridade">
                                            <?
                                            $escolaridades = Array();
                                            $escolaridades[] = Array("Ensino Fundamental - Cursando", "Ensino Fundamental - Completo");
                                            $escolaridades[] = Array("Ensino Médio - Cursando", "Ensino Médio - Completo");
                                            $escolaridades[] = Array("Ensino Superior - Cursando", "Ensino Superior - Completo");
                                            $escolaridades[] = Array("Pós Graduação - Cursando", "Pós Graduação - Completo");
                                            $escolaridades[] = Array("Mestrado - Cursando", "Mestrado - Completo");
                                            ?>
                                            <? foreach ($escolaridades as $grupo): ?>
                                                <optgroup>
                                                    <option<?= $funcionario['escolaridade'] == $grupo[0] ? ' selected' : '' ?>><?= $grupo[0] ?></option>
                                                    <option<?= $funcionario['escolaridade'] == $grupo[1] ? ' selected' : '' ?>><?= $grupo[1] ?></option>
                                                </optgroup>
                                            <? endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="sexo">Estado Civil:</label>
                                        <select class="form-control" name="estado_civil" id="estado_civil">
                                            <? $estadoscivis = Array("Solteiro", "Casado", "Divorciado", "Viúvo"); ?>
                                            <? foreach ($estadoscivis as $ec): ?>
                                                <option<?= $ec == $funcionario['estado_civil'] ? ' selected' : '' ?>><?= $ec ?></option>
                                            <? endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="cpf">CPF:</label>
                                        <input type="text" class="form-control" id="cpf" name="cpf" value="<?= $funcionario['cpf'] ?>"/>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="rg">RG:</label>
                                        <input type="text" class="form-control" id="rg" name="rg" value="<?= $funcionario['rg'] ?>" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <label for="ctps">CTPS:</label>
                                        <input type="text" class="form-control" name="ctps" id="ctps" value="<?= $funcionario['ctps'] ?>" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="titulo_de_eleitor">Titulo de Eleitor:</label>
                                        <input type="text" class="form-control" name="titulo_de_eleitor" id="titulo_de_eleitor" value="<?= $funcionario['titulo_de_eleitor'] ?>" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="habilitacao">Carteira de Habilitação:</label>
                                        <input type="text" class="form-control" name="habilitacao" id="habilitacao" value="<?= $funcionario['habilitacao'] ?>" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="reservista">Carteira de Reservista:</label>
                                        <input type="text" class="form-control" name="reservista" id="reservista" value="<?= $funcionario['reservista'] ?>" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Dados Admissionais
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <label for="matricula">Matrícula:</label>
                                        <input type="text" class="form-control" name="matricula" id="matricula" value="<?= $funcionario['matricula'] ?>" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="funcao">Função:</label>
                                        <input type="text" class="form-control" name="funcao" id="funcao" value="<?= $funcionario['funcao'] ?>" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="data_de_admissao">Data de Admissão:</label>
                                        <input type="date" class="form-control" name="data_de_admissao" id="data_de_admissao" value="<?= format_date($funcionario['data_de_admissao']) ?>" />
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="tempo_experiencia">Tempo de Experiência:</label>
                                        <input type="text" class="form-control" name="tempo_experiencia" id="tempo_experiencia" value="<?= $funcionario['tempo_experiencia'] ?>" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <label for="setor">Setor:</label>
                                        <select class="form-control" name="setor" id="setor">
                                            <? foreach ($setores as $setor): ?>
                                                <option value="<?= $setor['id'] ?>"<?= $setor['id'] == $funcionario['setor'] ? ' selected' : '' ?>><?= $setor['nome'] ?></option>
                                            <? endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="empresa">Empresa:</label>
                                        <select class="form-control" id="empresa" name="empresa">
                                            <? foreach ($empresas as $empresa): ?>
                                                <option value="<?= $empresa['id'] ?>"<?= $empresa['id'] == $funcionario['empresa'] ? ' selected' : '' ?>><?= $empresa['nome'] ?></option>
                                            <? endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="horario">Horário:</label>
                                        <input type="text" class="form-control" name="horario" id="horario" value="<?= $funcionario['horario'] ?>" />
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="salario">Salário:</label>
                                        <input type="text" class="form-control" name="salario" id="salario" value="<?= $funcionario['salario'] ?>" />
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="vale_transporte">Vale Transporte:</label>
                                        <input type="text" class="form-control" name="vale_transporte" id="vale_transporte" value="<?= $funcionario['vale_transporte'] ?>" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="obs">Observação:</label>
                                        <textarea class="form-control" name="obs" id="obs" rows="5"><?= $funcionario['obs'] ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-success btn-block">Salvar Alterações</button>
                        <br>
                    </div>
                </div>
            </form>
        </div>
    <? else: redirect(base_url('funcionarios')) ?>
    <? endif; ?>
</div>