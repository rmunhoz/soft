<?

function gimme_money($value) {
    return 'R$ ' . number_format($value, 2, ",", ".");
}

function format_date($date) {
    $date = strtotime($date);
    return date('d/m/Y', $date);
}

$funcionario = $funcionario[0];
foreach ($funcionario as $prop => $value) {
    if ($value == '' && $prop != "obs") {
        $funcionario[$prop] = 'Não disponível';
    } elseif ($value == '' && $prop == "obs") {
        $funcionario['obs'] = '-';
    }
}
?>
<div class="container">
    <? if ($funcionario != null): ?>
        <div class="page-header">
            <?
            if (isset($result)) {
                $result = json_decode($result);
            }
            ?>
            <? if (isset($result)): ?>
                <? if ($result->result): ?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?= $result->message ?>
                    </div>
                <? else: ?>
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?= $result->message ?>
                    </div>
                <? endif; ?>
            <? endif; ?>
            <h1>
                <small><i class="fa fa-info-circle"></i> Detalhes do Funcionário </small>
                <?= $funcionario['nome'] ?> <?= $funcionario['sobrenome'] ?>
                <? if ($funcionario['ativo'] != 1): ?>
                    <div class="badge">Inativo</div>
                <? endif; ?>
                <div class="pull-right">
                    <a class="btn btn-primary" href="<?= base_url("/funcionarios/editar?id={$funcionario['id']}") ?>">
                        <i class="fa fa-pencil"></i> Editar
                    </a>
                </div>
            </h1>
        </div>
        <?= $this->input->get('dump') == 1 ? var_dump($funcionario) : '' ?>
        <div class="col-md-12">
            <form class="form">
                <div class="row">
                    <div class="col-md-2">
                        <div class="row">
                            <img src="http://placehold.it/150x200" width="100%">
                        </div>
                        <br>
                    </div>
                    <div class="col-md-10">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Informações Básicas
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <label for="name">Nome:</label>
                                        <p id="nome"><?= $funcionario['nome'] ?></p>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="sobrenome">Sobrenome:</label>
                                        <p id="sobrenome"><?= $funcionario['sobrenome'] ?></p>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="data_nascimento">Data de Nascimento</label>
                                        <p id="data_nascimento"><?= format_date($funcionario['data_nascimento']) ?></p>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="sexo">Sexo:</label>
                                        <p id="sexo"> <?= $funcionario['sexo'] ?></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 form-group">
                                        <label for="local_de_nascimento">Cidade Natal:</label>
                                        <p id="local_de_nascimento" ><?= $funcionario['local_de_nascimento'] ?></p>
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="telefone">Telefone:</label>
                                        <p id="telefone"><?= $funcionario['telefone'] ?></p>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="celular">Celular:</label>
                                        <p id="celular"><?= $funcionario['celular'] ?></p>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label for="email">Email:</label>
                                        <p id="email" ><?= $funcionario['email'] ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Endereço
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-2 form-group">
                                        <label for="cep">CEP:</label>
                                        <p id="cep" name="cep" ><?= $funcionario['cep'] ?></p>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="endereco">Rua:</label>
                                        <p id="endereco" ><?= $funcionario['endereco'] ?></p>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="numero">Número:</label>
                                        <p id="numero" name="numero" ><?= $funcionario['numero'] ?></p>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="municipio">Município:</label>
                                        <p id="municipio" name="municipio" ><?= $funcionario['municipio'] ?></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <label for="bairro">Bairro:</label>
                                        <p id="bairro" name="bairro" ><?= $funcionario['bairro'] ?></p>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <label for="complemento">Complemento:</label>
                                        <p id="complemento" name="complemento" ><?= $funcionario['complemento'] ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Dados Pessoais
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="mae">Nome da Mãe:</label>
                                        <p id="mae" ><?= $funcionario['mae'] ?></p>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="pai">Nome do Pai:</label>
                                        <p id="pai" ><?= $funcionario['pai'] ?></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <label for="escolaridade">Escolaridade:</label>
                                        <p id="escolaridade"><?= $funcionario['escolaridade'] ?></p>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="estado_civil">Estado Civil:</label>
                                        <p id="estado_civil"><?= $funcionario['estado_civil'] ?></p>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="cpf">CPF:</label>
                                        <p id="cpf" name="cpf" ><?= $funcionario['cpf'] ?></p>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="rg">RG:</label>
                                        <p id="rg" name="rg" ><?= $funcionario['rg'] ?></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <label for="ctps">CTPS:</label>
                                        <p id="ctps" ><?= $funcionario['ctps'] ?></p>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="titulo_de_eleitor">Titulo de Eleitor:</label>
                                        <p id="titulo_de_eleitor" ><?= $funcionario['titulo_de_eleitor'] ?></p>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="habilitacao">Carteira de Habilitação:</label>
                                        <p id="habilitacao" ><?= $funcionario['habilitacao'] ?></p>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="reservista">Carteira de Reservista:</label>
                                        <p id="reservista" ><?= $funcionario['reservista'] ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Dados Admissionais
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <label for="matricula">Matrícula:</label>
                                        <p id="matricula" ><?= $funcionario['matricula'] ?></p>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="funcao">Função:</label>
                                        <p id="funcao" ><?= $funcionario['funcao'] ?></p>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="data_de_admissao">Data de Admissão:</label>
                                        <p id="data_de_admissao" ><?= format_date($funcionario['data_de_admissao']) ?></p>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="tempo_experiencia">Tempo de Experiência:</label>
                                        <p id="tempo_experiencia" ><?= $funcionario['tempo_experiencia'] ?></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 form-group">
                                        <label for="setor">Setor:</label>
                                        <p id="setor" ><?= $funcionario['setor'] ?></p>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="empresa">Empresa:</label>
                                        <p id="empresa" ><?= $funcionario['empresa'] ?></p>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="horario">Horário:</label>
                                        <p id="horario" ><?= $funcionario['horario'] ?></p>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="aalario">Salário:</label>
                                        <p id="aalario" ><?= gimme_money($funcionario['salario']) ?></p>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <label for="vale_transporte">Vale Transporte:</label>
                                        <p id="vale_transporte" ><?= $funcionario['vale_transporte'] ?></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="obs">Observação:</label>
                                        <p id="obs" rows="5"><?= $funcionario['obs'] ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    <? else: redirect(base_url('funcionarios')) ?>
    <? endif; ?>
</div>