<?php

class MY_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $logado = $this->session->userdata("user");

        $postkey = "*)O./°3¢¬n¬a./UExVuQV%G;;uD]CF42¨`IeG¹}+(Oqa]{;Mhs";
        $getkey = "9ZGjx7UahhSuaS5QKEvRaifsGT3WLkH4UIwxbS3DJGp93bHNLX";

        if ($logado == "" || !(isset($logado))) {
            if ($this->input->post('key') !== $postkey && $this->input->get('key') !== $getkey) {
                $url = base_url('login');
                redirect($url);
            }
        }
    }

}
