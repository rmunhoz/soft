<?php

class Page {

    public $title, $url, $name;

    function __construct() {
        
    }

    /**
     * 
     * @param String $title
     * @param String $url
     * @param String $name
     * @return Page Objeto Page contendo informações sobre determinada página
     */
    function cPage($title, $url, $name) {
        $page = new Page();
        $page->title = $title;
        $page->url = $url;
        $page->name = $name;
        return $page;
    }

}
