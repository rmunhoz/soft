var app = angular.module('Empresas', []);
var needsReload = false;

app.controller('ListController', function ($scope) {

    $scope.selected = []
    $scope.hidden = [];
    this.idEdit = "";
    this.nomeEdit = "";
    $scope.processando = [];

    this.deleteEmpresa = function (id) {
        id = id | null;
        url = '/soft/empresas/delete/' + id;
        if (id < 1) {
            alert("Erro interno");
        } else {
            $.ajax({
                url: url,
                success: function (response) {
                    $scope.hidden.push(id);
                    location.reload();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Erro: " + textStatus + "\n" + errorThrown);
                },
                complete: function () {
                    $scope.$apply();
                }
            });
        }
    };

    this.shouldBeShown = function (id) {
        return $scope.hidden.indexOf(id) < 0;
    };

    this.select = function (id) {
        if (!this.selected(id)) {
            $scope.selected.push(id);
        }
    };

    this.unselect = function (id) {
        $scope.selected.splice($scope.selected.indexOf(id), 1);
    };

    this.selected = function (id) {
        return $scope.selected.indexOf(id) >= 0;
    };

    this.deleteSelected = function () {
        var deleted = [];
        $scope.selected.forEach(function (entry) {
            url = '/soft/empresas/delete/' + entry;
            $.ajax({
                url: url,
                success: function (response) {
                    $scope.hidden.push(entry);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Erro: " + textStatus + "\n" + errorThrown);
                },
                complete: function () {
                    $scope.$apply();
                    deleted.push(entry);
                    if (deleted.length == $scope.selected.length) {
                        location.reload();
                    }
                }
            });
        });
    };

    this.hasSelection = function () {
        return $scope.selected.length > 0;
    };

    this.editando = function (id) {
        return this.idEdit == id;
    };

    this.editar = function (id, nome) {
        this.idEdit = id;
        this.nomeEdit = nome;
    };

    this.finalizar = function () {
        if (this.nomeEdit != "") {
            $scope.processando.push(this.idEdit);
            $.ajax({
                url: "/soft/empresas/update",
                type: 'POST',
                data: {id: this.idEdit, nome: this.nomeEdit},
                success: function (response) {
                    if (response == "true") {
                        this.idEdit = "";
                        this.nomeEdit = "";
                        location.reload();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Erro ao concluir edição");
                },
                complete: function () {
                    var index = $scope.processando.indexOf(this.idEdit);
                    if (index >= 0) {
                        $scope.processando.splice(index, 1);
                    }
                    $scope.$apply();
                }
            });
        }
    };

    this.cancelarEdicao = function () {
        this.idEdit = "";
        this.nomeEdit = "";
    };

    this.finalizando = function (id) {
        return $scope.processando.indexOf(id) >= 0;
    };
});

app.controller('NavController', function ($scope) {

    this.active = "visualizar";

    this.setActive = function (page) {
        if (needsReload && page == "visualizar") {
            location.reload();
        } else {
            this.active = page;
        }
    };

    this.isActive = function (page) {
        return this.active === page;
    };
});

app.controller('CadastroController', function ($scope) {
    this.paraCadastrar = [];
    $scope.cadastrados = [];
    $scope.comErro = [];
    $scope.processando = [];

    this.nome = "";

    this.addEmpresa = function (nome) {
        this.paraCadastrar.push(nome);
        this.nome = "";
    };

    this.removeEmpresa = function (nome) {
        var errorIndex, successIndex, id, procIndex;
        id = this.paraCadastrar.indexOf(nome);
        errorIndex = $scope.comErro.indexOf(nome);
        successIndex = $scope.cadastrados.indexOf(nome);
        procIndex = $scope.processando.indexOf(nome);
        if (id >= 0) {
            this.paraCadastrar.splice(id, 1);
        }
        if (errorIndex >= 0) {
            $scope.comErro.splice(errorIndex, 1);
        }
        if (successIndex >= 0) {
            $scope.cadastrados.splice(successIndex, 1);
        }
    };

    this.finaliza = function () {
        this.paraCadastrar.forEach(function (entry) {
            if ($scope.cadastrados.indexOf(entry) < 0) {
                $scope.processando.push(entry);
                $.ajax({
                    url: "/soft/empresas/insert?nome=" + entry,
                    success: function (response) {
                        if (response == "true") {
                            $scope.cadastrados.push(entry);
                        } else {
                            $scope.comErro.push(entry);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $scope.comErro.push(entry);
                        console.log(errorThrown);
                    },
                    complete: function () {
                        var procIndex = $scope.processando.indexOf(entry);
                        if (procIndex >= 0) {
                            $scope.processando.splice(procIndex, 1);
                        }
                        needsReload = true;
                        $scope.$apply();
                    }
                });
            }
        });
    };

    this.success = function (nome) {
        return $scope.cadastrados.indexOf(nome) >= 0;
    };

    this.error = function (nome) {
        return $scope.comErro.indexOf(nome) >= 0;
    };

    this.deletable = function (nome) {
        return ($scope.cadastrados.indexOf(nome) < 0 && $scope.processando.indexOf(nome) < 0);
    };

    this.processing = function (nome) {
        nome = nome | "";
        if (nome == "") {
            return $scope.processando.length > 0;
        } else {
            return $scope.processando.indexOf(nome) >= 0;
        }
    };

    this.canSave = function () {
        return $scope.comErro.length > 0 || this.paraCadastrar.length > $scope.cadastrados.length;
    };

    this.limpa = function () {
        this.paraCadastrar = [];
        $scope.cadastrados = [];
        $scope.comErro = [];
        $scope.processando = [];
    };

});