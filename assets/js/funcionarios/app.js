var app = angular.module('Funcionarios', []);

var inArray = function (arr, query) {
    if (arr.constructor === Array) {
        return getIndex(arr, query) >= 0;
    } else {
        return null;
    }
};

var getIndex = function (arr, query) {
    if (arr.constructor === Array) {
        return arr.indexOf(query);
    } else {
        return -1;
    }
};

var setScope = function ($scope, value) {
    value = value || null;
    if (value != null) {
        $scope.selected = value;
    } else {
        $scope.selected = window.table.rows({selected: true})[0].length;
    }
    $scope.$apply();
};

function setCallbacks($scope) {

    setScope = function (e, dt, type, indexes) {
        $scope.selected = window.table.rows({selected: true})[0].length;
        $scope.$apply();
    };

    window.table.on('select', setScope);

    window.table.on('deselect', setScope);

}

app.controller('TableController', function ($scope) {

    $scope.processing = [];

    if (window.table === undefined) {
        setTimeout(function () {
            setCallbacks($scope);
            $('#tabFuncionarios').removeClass('hidden');
        }, 1000);
    } else {
        setCallbacks($scope);
        $('#tabFuncionarios').removeClass('hidden');
    }

    this.hasSelection = function () {
        return $scope.selected > 0;
    };

    this.deleteSelected = function () {
        var msg = "";

        if ($scope.selected > 1) {
            msg = "Excluir os %s funcionários selecionados?";
        } else {
            msg = "Excluir o funcionário selecionado?"
        }

        var sure = confirm(msg.replace('%s', $scope.selected));
        if (sure) {
            var data = window.table.rows({selected: true}).data();
            var toDelete = [];
            for (var i = 0; i < $scope.selected; i++) {
                toDelete.push(data[i][0]);
            }

            $('.selected button.btn-danger').hide();
            $('.selected .label').removeClass('ng-hide');
            $('.selected button').attr('disabled', '');

            url = '/soft/funcionarios/delete';

            $.ajax({
                url: url,
                type: 'POST',
                data: {'ids': toDelete},
                success: function (response) {
                    if (response == 'true') {
                        toDelete.forEach(function (element) {
                            $("[reg-id='%id']".replace('%id', element)).remove();
                        })
                        location.reload();
                    } else {
                        toDelete.forEach(function (id) {
                            if (inArray($scope.processing, id)) {
                                $scope.processing.splice(getIndex($scope.processing, id), 1);
                                $scope.$apply();
                            }
                        })
                        alert("Erro ao excluir\nResponse: " + response);
                    }
                },
                error: function () {
                    alert("Erro ao excluir (error callback)");
                },
                complete: function () {

                }
            });
        }
    };

    this.deleteOne = function (id) {
        if (confirm("Tem certeza que deseja excluir o funcionário?")) {
            $scope.processing.push(id);
            url = '/soft/funcionarios/delete';
            $.ajax({
                url: url,
                type: 'POST',
                data: {'id': id},
                success: function (response) {
                    if (response == 'true') {
                        $("[reg-id='%id']".replace('%id', id)).remove();
                        if (inArray($scope.processing, id)) {
                            $scope.processing.splice(getIndex($scope.processing, id), 1);
                        }
                        location.reload();
                    } else {
                        if (inArray($scope.processing, id)) {
                            $scope.processing.splice(getIndex($scope.processing, id), 1);
                            $scope.$apply();
                        }
                        alert("Erro ao excluir o registro");
                    }
                },
                error: function () {
                    alert("Erro ao excluir o registro");
                }
            });
        }
    };

    this.hasActions = function (id) {
        return !inArray($scope.processing, id);
    };

    this.inactivate = function (id) {
        if (confirm("Deseja realmente marcar o funcionário como inativo?")) {
            var lineSelector = "[reg-id='%id']".replace('%id', id);
            $(lineSelector + ' button.btn-warning').addClass('hidden');
            $(lineSelector + ' .label').removeClass('ng-hide');
            $(lineSelector + ' button').attr('disabled', '');
            var url = "/soft/funcionarios/inactivate";
            $.ajax({
                url: url,
                type: 'POST',
                data: {'id': id},
                success: function (response) {
                    if (response == 'true') {
                        location.reload();
                    } else {
                        alert("Erro ao marcar o funcionário como inativo");
                        var lineSelector = "[reg-id='%id']".replace('%id', id);
                        $(lineSelector + ' button.btn-warning').removeClass('hidden');
                        $(lineSelector + ' .label').addClass('ng-hide');
                        $(lineSelector + ' button').removeAttr('disabled');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Erro: " + errorThrown);
                    var lineSelector = "[reg-id='%id']".replace('%id', id);
                    $(lineSelector + ' button.btn-warning').removeClass('hidden');
                    $(lineSelector + ' .label').addClass('ng-hide');
                    $(lineSelector + ' button').removeAttr('disabled');
                }
            });
        }
    };

    this.inactivateSelected = function () {
        var msg = "";

        if ($scope.selected > 1) {
            msg = "Marcar os %s funcionários selecionados como inativos?";
        } else {
            msg = "Marcar o funcionário selecionado como inativo?"
        }

        var sure = confirm(msg.replace('%s', $scope.selected));
        if (sure) {
            var data = window.table.rows({selected: true}).data();
            var toInactivate = [];
            for (var i = 0; i < $scope.selected; i++) {
                var lineSelector = "[reg-id='%id']".replace('%id', data[i][0]);
                if ($(lineSelector).attr('reg-active') == 1) {
                    toInactivate.push(data[i][0]);
                }
            }

            if (toInactivate.length > 0) {

                $('.selected button.btn-warning').hide();
                $('.selected .label').removeClass('ng-hide');
                $('.selected button').attr('disabled', '');

                url = '/soft/funcionarios/inactivate';

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {'id': toInactivate},
                    success: function (response) {
                        if (response == 'true') {
                            location.reload();
                        } else {
                            toInactivate.forEach(function (id) {
                                if (inArray($scope.processing, id)) {
                                    $scope.processing.splice(getIndex($scope.processing, id), 1);
                                    $scope.$apply();
                                }
                            })
                            alert("Erro ao marcar\nResponse: " + response);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Erro ao marcar (error callback)\n" + errorThrown);
                    }
                });
            } else {
                alert("Você não selecionou nenhum funcionário ativo");
            }
        }
    };

    this.activate = function (id) {
        if (confirm("Deseja realmente marcar o funcionário como ativo?")) {
            var lineSelector = "[reg-id='%id']".replace('%id', id);
            $(lineSelector + ' button.btn-success').addClass('hidden');
            $(lineSelector + ' .label').removeClass('ng-hide');
            $(lineSelector + ' button').attr('disabled', '');
            var url = "/soft/funcionarios/activate";
            $.ajax({
                url: url,
                type: 'POST',
                data: {'id': id},
                success: function (response) {
                    if (response == 'true') {
                        location.reload();
                    } else {
                        alert("Erro ao marcar o funcionário como ativo\n" + response);
                        var lineSelector = "[reg-id='%id']".replace('%id', id);
                        $(lineSelector + ' button.btn-success').removeClass('hidden');
                        $(lineSelector + ' .label').addClass('ng-hide');
                        $(lineSelector + ' button').removeAttr('disabled');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Erro: " + errorThrown);
                    var lineSelector = "[reg-id='%id']".replace('%id', id);
                    $(lineSelector + ' button.btn-warning').removeClass('hidden');
                    $(lineSelector + ' .label').addClass('ng-hide');
                    $(lineSelector + ' button').removeAttr('disabled');
                }
            });
        }
    };

    this.activateSelected = function () {
        var msg = "";

        if ($scope.selected > 1) {
            msg = "Marcar os %s funcionários selecionados como ativos?";
        } else {
            msg = "Marcar o funcionário selecionado como ativo?"
        }

        var sure = confirm(msg.replace('%s', $scope.selected));
        if (sure) {
            var data = window.table.rows({selected: true}).data();
            var toActivate = [];
            for (var i = 0; i < $scope.selected; i++) {
                var lineSelector = "[reg-id='%id']".replace('%id', data[i][0]);
                if ($(lineSelector).attr('reg-active') != 1) {
                    toActivate.push(data[i][0]);
                }
            }

            if (toActivate.length > 0) {

                $('.selected button.btn-success').hide();
                $('.selected .label').removeClass('ng-hide');
                $('.selected button').attr('disabled', '');

                url = '/soft/funcionarios/activate';

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {'id': toActivate},
                    success: function (response) {
                        if (response == 'true') {
                            location.reload();
                        } else {
                            toActivate.forEach(function (id) {
                                if (inArray($scope.processing, id)) {
                                    $scope.processing.splice(getIndex($scope.processing, id), 1);
                                    $scope.$apply();
                                }
                            })
                            alert("Erro ao marcar\nResponse: " + response);
                        }
                    },
                    error: function () {
                        alert("Erro ao marcar (error callback)");
                    }
                });
            } else {
                alert("Você não selecionou nenhum funcionário inativo");
            }
        }
    };

});