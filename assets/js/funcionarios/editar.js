$(document).ready(function () {
    if (!Modernizr.inputtypes.date) {
        $('[type="date"]').datepicker();
    }
    $('#cpf').mask('000.000.000-00');
    $('#salario').mask('#.##0,00', {reverse: true, selectOnFocus: true});
    $('#vale_transporte').mask('0#');
    $('#telefone').mask('(00) 0000-0000');
    $('#celular').mask('(00) 0000-0000');
    $('#data_nascimento').mask('00/00/0000');
    $('#data_de_admissao').mask('00/00/0000');
    $('#cep').mask('00000-000');
});

var app = angular.module('editar', []);

app.controller('BtnController', function ($scope) {

    this.isTogglingActiveStatus = false;
    this.isDeleting = false;

    this.inactivate = function (id) {
        if (confirm('Deseja realmente marcar o funcionário como inativo?')) {
            this.isTogglingActiveStatus = true;
            var url = "/soft/funcionarios/inactivate";
            $.ajax({
                url: url,
                type: 'POST',
                success: function (response) {
                    if (response == 'true') {
                        location.reload();
                    } else {
                        alert("Erro ao marcar o funcionário como inativo");
                        this.isTogglingActiveStatus = false;
                    }
                },
                data: {'id': id},
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Erro: " + errorThrown);
                    this.isTogglingActiveStatus = false;
                }
            });
        }
    };

    this.activate = function (id) {
        this.isTogglingActiveStatus = true;
        if (confirm('Deseja realmente marcar o funcionário como ativo?')) {
            var url = "/soft/funcionarios/activate";
            $.ajax({
                url: url,
                type: 'POST',
                data: {'id': id},
                success: function (response) {
                    if (response == 'true') {
                        location.reload();
                    } else {
                        alert("Erro ao marcar o funcionário como ativo\n" + response);
                        this.isTogglingActiveStatus = false;
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Erro: " + errorThrown);
                    this.isTogglingActiveStatus = false;
                }
            });
        }
    };

    this.delete = function (id) {
        if (confirm('Deseja realmente excluir o funcionário? Esta ação não pode ser desfeita!')) {
            this.isDeleting = true;
            url = '/soft/funcionarios/delete';
            $.ajax({
                url: url,
                type: 'POST',
                data: {'id': id},
                success: function (response) {
                    if (response == 'true') {
                        alert("Funcionário excluído com sucesso");
                        window.location.href = '/soft/funcionarios';
                    } else {
                        alert("Erro ao excluir o registro");
                        this.isDeleting = false;
                    }
                },
                error: function () {
                    alert("Erro ao excluir o registro");
                    this.isDeleting = false;
                }
            });
        }
    };
});