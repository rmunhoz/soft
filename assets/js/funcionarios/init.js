$(document).ready(function () {
    window.table = $('#tabFuncionarios').DataTable({
        select: true,
        language: {
            "decimal": ",",
            "emptyTable": "Nenhum funcionário cadastrado",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "infoEmpty": "Mostrando 0 a 0 de 0 registros",
            "infoFiltered": "(filtrado de _MAX_ registros totais)",
            "infoPostFix": "",
            "thousands": ".",
            "lengthMenu": "Mostrar _MENU_ registros",
            "loadingRecords": "Carregando...",
            "processing": "Processando...",
            "search": "Pesquisar:",
            "zeroRecords": "Nenhum registro encontrado",
            "paginate": {
                "first": "Primeira",
                "last": "Última",
                "next": "Próxima",
                "previous": "Anterior"
            },
            "aria": {
                "sortAscending": ": ative para organizar por ordem crescente",
                "sortDescending": ": ative para organizar por ordem decrescente"
            },
            select: {
                rows: {
                    _: "%d linhas selecionadas",
                    0: "",
                    1: "Uma linha selecionada"
                }
            }
        },
        "columnDefs": [
            {
                "targets": [0],
                "visible": false
            },
            {
                "targets": [5],
                "orderable": false
            }
        ],
        "order": [
            [1, 'asc']
        ]
    });

    $('#tabFuncionarios')
            .removeClass('display')
            .addClass('table table-striped table-bordered table-hover');


    $('#tabFuncionarios').addClass('hidden');

});
