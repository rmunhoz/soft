var app = angular.module("Usuarios", []);

app.controller("PageController", function ($scope, $http) {

    this.selected = "visualizar";
    $scope.processing = [];

    this.select = function (what) {
        this.selected = what;
    };

    this.isSelected = function (what) {
        return this.selected === what;
    };

    this.resetSenha = function (login) {
        if (confirm("Deseja realmente redefinir a senha desse usuário?")) {

            var url = '/soft/resetSenha/reset';

            $scope.processing.push(login);

            $http.post(url, {
                "login": login,
                "json": 'true'
            }, {'headers': {'Content-Type': 'application/x-www-form-urlencoded'}}).then(
                    function successCallback(response) {
                        if (response.data != 'true') {
                            alert('Eita, ocorreu algum erro. Por favor, tente novamente!');
                            console.log(response);
                        } else {
                            alert("Um email de redefinição de senha foi enviado para o endereço cadastrado");
                        }
                    },
                    function errorCallback(response) {
                        console.log(response);
                        switch (response.status) {
                            case 500:
                                alert('Ops, ocorreu um erro ao enviar o email de redefinição :/ Por favor, tente novamente!');
                                break;
                            default:
                                alert('Eita, ocorreu um erro estranho: ' + response.status + ' - ' + response.statusText);
                                break;
                        }
                    }
            ).finally(function () {
                if ($scope.processing.indexOf(login) >= 0) {
                    $scope.processing.splice($scope.processing.indexOf(login));
                }
            });
        }
    };


    this.isProcessing = function (login) {
        return $scope.processing.indexOf(login) >= 0;
    };

    function spliceIfExists(array, index) {
        if (array.indexOf(index) >= 0) {
            array.splice(array.indexOf(index));
        }
    }

    this.deleteUser = function (login) {
        if (confirm("Deseja realmente excluir o usuário?")) {
            $scope.processing.push(login);

            $http.post('/soft/usuarios/delete', {
                'login': login
            }, {'headers': {'Content-Type': 'application/x-www-form-urlencoded'}}).then(
                    function successCallback(response) {
                        if (response.data != 'true') {
                            alert('Eita, ocorreu algum erro. Por favor, tente novamente!');
                            console.log(response);
                        } else {
                            alert("Usuário excluído com sucesso!");
                            location.reload();
                        }
                    },
                    function errorCallback(response) {
                        console.log(response);
                        switch (response.status) {
                            case 500:
                                alert('Ops, ocorreu um erro ao excluir o usuário :/ Por favor, tente novamente!');
                                break;
                            default:
                                alert('Eita, ocorreu um erro estranho: ' + response.status + ' - ' + response.statusText);
                                break;
                        }
                    }
            ).finally(function () {
                spliceIfExists($scope.processing, login);
            });
        }
    };
});