$(document).ready(function () {
    var table = $('#tabUsuarios').DataTable({
        select: {
            style: 'single',
            selector: 'td:not(td:last-child)'
        },
        language: {
            "decimal": ",",
            "emptyTable": "Nenhum usuário cadastrado",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "infoEmpty": "Mostrando 0 a 0 de 0 registros",
            "infoFiltered": "(filtrado de _MAX_ registros totais)",
            "infoPostFix": "",
            "thousands": ".",
            "lengthMenu": "Mostrar _MENU_ registros",
            "loadingRecords": "Carregando...",
            "processing": "Processando...",
            "search": "Pesquisar:",
            "zeroRecords": "Nenhum registro encontrado",
            "paginate": {
                "first": "Primeira",
                "last": "Última",
                "next": "Próxima",
                "previous": "Anterior"
            },
            "aria": {
                "sortAscending": ": ative para organizar por ordem crescente",
                "sortDescending": ": ative para organizar por ordem decrescente"
            },
            select: {
                rows: {
                    _: "%d linhas selecionadas",
                    0: "",
                    1: "Uma linha selecionada"
                }
            }
        },
        columnDefs: [
            {
                targets: [4],
                orderable: false
            }
        ],
        order: [
            [0, 'asc']
        ]
    });

    $('#tabUsuarios')
            .removeClass('display')
            .addClass('table table-striped table-bordered table-hover');
});
